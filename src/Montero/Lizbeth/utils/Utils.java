package Montero.Lizbeth.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class Utils {
    public static String[] getProperties() throws Exception {
        Properties prop = new Properties();
        String[] dbConfig = new String[2];
        prop.load(new FileInputStream("db.props"));
        dbConfig[0] = prop.getProperty("driver");
        dbConfig[1] = prop.getProperty("url");
        return dbConfig;
    }
    public static int getMotorBD() throws Exception{
        Properties prop = new Properties();
        prop.load(new FileInputStream("db.props"));
        return Integer.parseInt(prop.getProperty("motor"));
    }
}
