package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.categoria.ICategoriaDAO;
import Montero.Lizbeth.bl.categoria.SqlServerCategoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.coleccionista.IColeccionistaDAO;
import Montero.Lizbeth.bl.coleccionista.SqlServerColeccionista;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerColeccionista {
    private IColeccionistaDAO dao;// encapsulamiento
    private DAOFactory factory;

    public ControllerColeccionista() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getColeccionista();
    }


  /*  public String registrarColeccionista(String nombre, String tipo, String password, String identificacion, int edad, String avatar, String estado, String correo, String direccion, String moderador, double calificacion, LocalDate fechaNacimiento) {
        Coleccionista c = new Coleccionista(identificacion,tipo,nombre,avatar,fechanacimiento,edad,correo,contrasenia,estado,calificacion,dirreccion,esModerador,subasta_codigo);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (ICategoriaDAO) factory.getCategoria();

            return dao.insertar(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
*/

    public String[] listarColeccionista() {
        String[] data;
        SqlServerColeccionista coleccionistaDao = new SqlServerColeccionista();
        int i = 0;
        try {
            ArrayList<String> listaColeccionista = coleccionistaDao.listarColeccionistas();
            data = new String[listaColeccionista.size()];
            for (String s : listaColeccionista) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }

//TODO REVISAR QUE FUNCIONE CORRECTAMENTE


    public String buscarColeccionista(String identificacion) throws Exception {

        Coleccionista c = dao.buscarColeccionista(identificacion);

        if (c != null) {
            return c.toString();
        }
        return "El codigo no existe";
    }


  /*  public String modificarColeccionista(String codigo, String nombre) throws Exception {
        Categoria cat = new Categoria(codigo, nombre);
        return dao.modificarCategoria(cat);
    }
*/
    public String eliminarColeccionista(String identificacion) throws Exception {
        Coleccionista c = dao.buscarColeccionista(identificacion);
        if (c != null) {
            return dao.eliminarColeccionista(identificacion);
        }
        return "La identificacion del coleccionista  no existe";
    }



}
