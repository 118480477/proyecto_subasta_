package Montero.Lizbeth.tl;


import Montero.Lizbeth.bl.interes.IInteresDAO;
import Montero.Lizbeth.bl.interes.Interes;
import Montero.Lizbeth.bl.interes.SqlServerInteres;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControllerInteres {
    private IInteresDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<Interes> intereses;


    public ControllerInteres() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getInteres();
    }


    // el controller hace el amarre, por eso no se utilizan los if
    public String registrarInteres(String codigo, String nombre,String descripcion) {
        Interes c = new Interes(codigo, nombre,descripcion);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IInteresDAO) factory.getInteres();

            return dao.insertar(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public String[] listarInteres() {
        String[] data;
        SqlServerInteres interesDao = new SqlServerInteres();
        int i = 0;
        try {
            ArrayList<String> listaInteres = interesDao.listar();
            data = new String[listaInteres.size()];
            for (String s : listaInteres) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }
    public String modificarInteres(String codigo, String nombre, String descripcion) throws Exception {
        Interes interes = new Interes(codigo, nombre, descripcion);
        return dao.modificarInteres(interes);
    }

    public String buscarInteres(String codigo) throws Exception {
        Interes interes = dao.buscarInteres(codigo);
        if (interes != null) {
            return interes.toString();
        }
        return "El código del interés no existe";
    }

    public String eliminarInteres(String codigo) throws Exception {
       Interes in =dao.buscarInteres(codigo);
        if (in != null) {
            return dao.eliminar(codigo);
        }
        return "El código del interés no existe";
    }
}
