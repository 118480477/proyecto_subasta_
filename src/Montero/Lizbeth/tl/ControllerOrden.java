package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.ordenDeCompra.IOrdenDeCompraDAO;
import Montero.Lizbeth.bl.ordenDeCompra.OrdenDeCompra;
import Montero.Lizbeth.bl.ordenDeCompra.SqlServerOrden;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerOrden {
    private IOrdenDeCompraDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<String> ordenes;


    public ControllerOrden() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getOrdenDeCompra();
    }



    // el controller hace el amarre, por eso no se utilizan los if
    public String registrarOrden(String codigo, LocalDate fecha,String precioTotal, String usuario_identififcacion) {
        OrdenDeCompra c = new OrdenDeCompra(codigo,fecha,precioTotal,usuario_identififcacion);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IOrdenDeCompraDAO) factory.getOrdenDeCompra();

            return dao.insertar(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public String[] listarOrden() {
        String[] data;
        SqlServerOrden ordenDao = new SqlServerOrden();
        int i = 0;
        try {
            ArrayList<String> listaOrden = ordenDao.listarOrden();
            data = new String[listaOrden.size()];
            for (String s : listaOrden) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }
//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarOrden(String codigo) throws Exception {
        OrdenDeCompra orden = dao.buscarOrden(codigo);
        if (orden != null) {
            return orden.toString();
        }
        return null;
    }

    public String modificarOrden(String codigo, LocalDate fecha,String precioTotal, String usuario_identififcacion) throws Exception {
        OrdenDeCompra o = new OrdenDeCompra( codigo,  fecha, precioTotal,  usuario_identififcacion);
        return dao.modificarOrden(o);
    }

    public String eliminarOrden(String codigo) throws Exception {
        OrdenDeCompra o= dao.buscarOrden(codigo);
        if (o != null) {
            return dao.eliminarOrden(codigo);
        }
        return "El codigo de la categoria no existe";
    }

}
