package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.categoria.ICategoriaDAO;
import Montero.Lizbeth.bl.categoria.SqlServerCategoria;
import Montero.Lizbeth.bl.interes.Interes;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControllerCategoria {
    private ICategoriaDAO dao;// encapsulamiento
    private DAOFactory factory;
    //private ArrayList<Categoria> categorias;


    public ControllerCategoria() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getCategoria();
    }


    // el controller hace el amarre, por eso no se utilizan los if

    public String registrarCategoria(String codigo, String nombre) {
        Categoria c = new Categoria(codigo, nombre);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (ICategoriaDAO) factory.getCategoria();

            return dao.insertar(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public String[] listarCategoria() {
        String[] data;
        SqlServerCategoria categoriaDao = new SqlServerCategoria();
        int i = 0;
        try {
            ArrayList<String> listaCategoria = categoriaDao.listar();
            data = new String[listaCategoria.size()];
            for (String s : listaCategoria) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }

//TODO REVISAR QUE FUNCIONE CORRECTAMENTE


    public String buscarCategoria(String codigo) throws Exception {

        Categoria cate = dao.buscarCategoria(codigo);

        if (cate != null) {
            return cate.toString();
        }
        return "El codigo no existe";
    }


    public String modificarCategoria(String codigo, String nombre) throws Exception {
        Categoria cat = new Categoria(codigo, nombre);
        return dao.modificarCategoria(cat);
    }

    public String eliminarCategoria(String codigo) throws Exception {
        Categoria cat = dao.buscarCategoria(codigo);
        if (cat != null) {
            return dao.eliminar(codigo);
        }
        return "El codigo de la categoria no existe";
    }

}
