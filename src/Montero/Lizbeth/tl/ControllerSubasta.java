package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.subasta.ISubastaDAO;
import Montero.Lizbeth.bl.subasta.SqlServerSubasta;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerSubasta {
    private ISubastaDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<String> subastas;



    public ControllerSubasta() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getSubasta();
    }


    // el controller hace el amarre, por eso no se utilizan los if

    public String registrarSubasta(String codigo,LocalDate fechaCreacion,LocalDate fechaInicio,int horaInicio,LocalDate fechaVencimiento,int horaVencimiento,String estado,double precioBase,String ordencompra_codigo) {
        Subasta s = new Subasta( codigo, fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, horaVencimiento, estado, precioBase, ordencompra_codigo);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (ISubastaDAO) factory.getSubasta();

            return dao.insertarSubasta(s);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public String[] listarSubasta() {
        String[] data;
        SqlServerSubasta subastaDao = new SqlServerSubasta();
        int i = 0;
        try {
            ArrayList<String> listarSubasta = subastaDao.listarSubasta();
            data = new String[listarSubasta.size()];
            for (String s : listarSubasta) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }
//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarSubasta(String codigo) throws Exception {
        Subasta s = dao.buscarSubasta(codigo);
        if (s != null) {
            return s.toString();
        }
        return "El código de la subasta  no existe";
    }


    public String modificarSubasta(String codigo,LocalDate fechaCreacion,LocalDate fechaInicio,int horaInicio,LocalDate fechaVencimiento,int horaVencimiento,String estado,double precioBase,String ordencompra_codigo) throws Exception {
        Subasta s = new Subasta( codigo, fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, horaVencimiento, estado, precioBase, ordencompra_codigo);
        return  dao.modificarSubasta(s);
    }
    public String eliminarSubasta(String codigo) throws Exception {
        Subasta  s = dao.buscarSubasta(codigo);
        if (s != null) {
            return dao.eliminarSubasta(codigo);
        }
        return "El codigo de la subasta no existe";
    }

}
