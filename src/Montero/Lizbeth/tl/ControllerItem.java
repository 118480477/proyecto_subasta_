package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;

import Montero.Lizbeth.bl.item.IItemDAO;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.item.SqlServerItem;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerItem {
    private IItemDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<Categoria> categorias;


    public ControllerItem() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getItem();
    }

    // el controller hace el amarre, por eso no se utilizan los if
    public String registrarItem(String codigo, String nombre,String descripcion, String estado, String imagen, LocalDate fechaCompra, LocalDate fechaAntiguedad,String codigo1, String categoria_codigo) {
        Item i = new Item( codigo,  nombre, descripcion,  estado,  imagen,  fechaCompra,  fechaAntiguedad, codigo1,  categoria_codigo);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IItemDAO) factory.getItem();

            return dao.insertar(i);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }


    public String[] listarItem() {
        String[] data;
        SqlServerItem itemDao = new SqlServerItem();
        int i = 0;
        try {
            ArrayList<String> listaitem = itemDao.listar();
            data = new String[listaitem.size()];
            for (String s : listaitem) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }
//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarItem(String codigo) throws Exception {
        String it = dao.buscarItem(codigo);
        if (it != null) {
            return it.toString();
        }
        return "El código del objeto  no existe";
    }
    public String modificarItem(String codigo, String nombre,String descripcion, String estado, String imagen, LocalDate fechaCompra, LocalDate fechaAntiguedad,String codigo1, String categoria_codigo ) throws Exception {
        Item it = new Item( codigo,  nombre, descripcion,  estado,  imagen,  fechaCompra,  fechaAntiguedad, codigo1,  categoria_codigo);
        return  dao.modificarItem(it);
    }
    public String eliminarItem(String codigo) throws Exception {
        String i = dao.buscarItem(codigo);
        if (i != null) {
            return dao.eliminarItem(codigo);
        }
        return "El codigo de la categoria no existe";
    }

}


