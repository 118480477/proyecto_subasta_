package Montero.Lizbeth.bl.oferta;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IOfertaDAO {

    String insertar(Oferta of) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<Oferta> listar() throws ClassNotFoundException,SQLException,Exception;
    void eliminar(int codigo);


}
