package Montero.Lizbeth.bl.oferta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Oferta y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */
public class Oferta {
    private int idOferente;
    private String codigo,subastaCodigo;
    private double puntuacionOferente, precioOfertado;
    private Item itemSubastado;
    private LocalDate fechaOferta;
    private Coleccionista coleccionistas; //revisar
    private ArrayList<Coleccionista> colecOfertando;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     *
     * @param idOferente         tipo int, identificacion de la oferta.
     * @param precioOfertado     tipo double, precio ofrecido por el coleccionista.
     * @param fechaOferta        tipo LocalDate, fecha en la que se hace la oferta.
     * @param itemSubastado    tipo Item, objeto que se subasta.
     * @param coleccionistas     tipo Coleccionista, coleccionista que hace la oferta.
     * @param puntuacionOferente tipo double, puntuacion que le dan a la oferta.
     */
    public Oferta(int idOferente, double precioOfertado, LocalDate fechaOferta, Item itemSubastado, Coleccionista coleccionistas, double puntuacionOferente) {
    }

    /**
     * Constructor con los parametros propios de la clase Oferta
     *
     * @param idOferente         tipo int, codigo de oferta.
     * @param puntuacionOferente tipo double, la puntuacion del coleccionista.
     * @param precioOfertado     tipo double, precio que le ofrecen a un articulo.
     * @param fechaOferta        tipo LocalDate, fecha en que se hace la oferta.
     */
    public Oferta(int idOferente, double puntuacionOferente, double precioOfertado, LocalDate fechaOferta) {
        this.idOferente = idOferente;
        this.puntuacionOferente = puntuacionOferente;
        this.precioOfertado = precioOfertado;
        this.fechaOferta = fechaOferta;
    }

    /**
     * Constructor con los parametros propios de la clase Oferta y sus respectivas relaciones.
     *
     * @param idOferente         tipo int, codigo de oferta.
     * @param puntuacionOferente tipo double, la puntuacion del coleccionista.
     * @param precioOfertado     tipo double, precio que le ofrecen a un articulo.
     * @param fechaOferta        tipo LocalDate, fecha en que se hace la oferta
     * @param coleccionistas     tipo Coleccionista, coleccionista que hace la oferta.
     * @param itemSubastado    tipo Item, objeto al que se le está haciendo la oferta.
     */
    public Oferta(int idOferente, double puntuacionOferente, double precioOfertado, Item itemSubastado, LocalDate fechaOferta, Coleccionista coleccionistas) {
        this.idOferente = idOferente;
        this.puntuacionOferente = puntuacionOferente;
        this.precioOfertado = precioOfertado;
        this.itemSubastado = itemSubastado;
        this.fechaOferta = fechaOferta;
        this.coleccionistas = coleccionistas;
    }

    public Oferta() {

    }

    /**
     * Metodo Getter de la variable que almacena el idOferente , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el idOferente , de la oferta lo hace publico.
     */
    public int getIdOferente() {
        return idOferente;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param idOferente: dato que almacena  el idOferentea, de la oferta de manera privada en su calse respectiva.
     */
    public void setIdOferente(int idOferente) {
        this.idOferente = idOferente;
    }

    /**
     * Metodo Getter de la variable que almacena la puntuacionOferente , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacionOferente  , de la oferta lo hace publico.
     */
    public double getPuntuacionOferente() {
        return puntuacionOferente;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacionOferente : dato que almacena  la puntuacionOferente , de la oferta de manera privada en su calse respectiva.
     */
    public void setPuntuacionOferente(double puntuacionOferente) {
        this.puntuacionOferente = puntuacionOferente;
    }

    /**
     * Metodo Getter de la variable que almacena el precioOfertado , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precioOfertado   , de la oferta lo hace publico.
     */
    public double getPrecioOfertado() {
        return precioOfertado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioOfertado : dato que almacena el precioOfertado  , de la oferta de manera privada en su calse respectiva.
     */
    public void setPrecioOfertado(double precioOfertado) {
        this.precioOfertado = precioOfertado;
    }

    /**
     * Metodo Getter de la variable que almacena el itemSubastado , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el itemSubastado   , de la oferta lo hace publico.
     */
    public Item getItemSubastado() {
        return itemSubastado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param itemSubastado : dato que almacena el itemSubastado  , de la oferta de manera privada en su calse respectiva.
     */
    public void setItemSubastado(Item itemSubastado) {
        this.itemSubastado = itemSubastado;
    }

    /**
     * Metodo Getter de la variable que almacena la fechaOferta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fechaOferta    , de la oferta lo hace publico.
     */
    public LocalDate getFechaOferta() {
        return fechaOferta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaOferta : dato que almacena la fechaOferta   , de la oferta de manera privada en su calse respectiva.
     */
    public void setFechaOferta(LocalDate fechaOferta) {
        this.fechaOferta = fechaOferta;
    }

    /**
     * Metodo Getter de la variable que almacena el coleccionista , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el coleccionista   , de la oferta lo hace publico.
     */
    public Coleccionista getColeccionistas() {
        return coleccionistas;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param coleccionistas : dato que almacena el coleccionista   , de la oferta de manera privada en su calse respectiva.
     */
    public void setColeccionistas(Coleccionista coleccionistas) {
        this.coleccionistas = coleccionistas;
    }


    public ArrayList<Coleccionista> getColecOfertando() {
        return colecOfertando;
    }

    public void setColecOfertando(ArrayList<Coleccionista> colecOfertando) {
        this.colecOfertando = colecOfertando;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSubastaCodigo() {
        return subastaCodigo;
    }

    public void setSubastaCodigo(String subastaCodigo) {
        this.subastaCodigo = subastaCodigo;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la oferta,con el respectivo valor de cada atributo en un solo String.
     */



    @Override
    public String toString() {
        return "Oferta{" +
                "idOferente=" + idOferente +
                ", puntuacionOferente=" + puntuacionOferente +
                ", precioOfertado=" + precioOfertado +
                ", itemSubastado=" + itemSubastado +
                ", fechaOferta=" + fechaOferta +
                ", coleccionistas=" + coleccionistas +
                '}';
    }

    public void add(Oferta tmpOferta) {
    }
}
