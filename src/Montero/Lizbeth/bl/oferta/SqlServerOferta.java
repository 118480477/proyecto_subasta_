package Montero.Lizbeth.bl.oferta;

//import Montero.Lizbeth.Accesobd.Conector;

import Montero.Lizbeth.Accesobd.Conector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerOferta implements IOfertaDAO {

    private String query;
    private ArrayList<Oferta> ofertas;


    public SqlServerOferta() {
    }

    public String insertar(Oferta o) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO oferta (codigo,precioOfertado,fechaOferta,subasta_codigo) VALUES (" +
                "'" + o.getCodigo() + "','" + o.getPrecioOfertado() +
                "'," +o.getFechaOferta() + "','" + o.getSubastaCodigo()+ ")";
        //Conector.getConector(Montero.Lizbeth.utils.Utils.getProperties()[0],Montero.Lizbeth.utils.Utils.getProperties()[1]).ejecutarQuery(query);
        return "Orden de compra registrada exitosamente";
    }

    public ArrayList<Oferta> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo,precioOfertado,fechaOferta,subasta_codigo FROM oferta";
        ofertas = new ArrayList<>();
        ResultSet rs = null;
        Conector.getConector(Montero.Lizbeth.utils.Utils.getProperties()[0], Montero.Lizbeth.utils.Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Oferta tmpOferta = new Oferta();
            tmpOferta.setCodigo(rs.getString("codigo"));
            tmpOferta.setPrecioOfertado(Double.parseDouble(rs.getString("precioOfertado")));
            tmpOferta.setFechaOferta(LocalDate.parse(rs.getString("fechaOferta")));
            tmpOferta.setSubastaCodigo(rs.getString("subasta_codigo"));
            tmpOferta.add(tmpOferta);
        }
        return ofertas;
    }

    @Override
    public void eliminar(int codigo) {

    }
}
