package Montero.Lizbeth.bl.administrador;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAdministradorDAO {
    String insertar(Administrador a) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;

}
