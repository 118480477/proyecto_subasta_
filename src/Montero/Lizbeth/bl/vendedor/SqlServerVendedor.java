package Montero.Lizbeth.bl.vendedor;

import Montero.Lizbeth.Accesobd.Conector;

import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerVendedor  implements  IVendedorDAO {
    private String query;
    private ArrayList<String> vendedores;

    @Override
    public String insertarVendedor(Vendedor v) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO vendedor (identificacion,nombre,apellido,correo,direccion,puntuacion) VALUES ('" + v.getIdentificacion() +
                "','" + v.getNombre() + "','" + v.getApellido() + "','" + v.getCorreo() + "','" + v.getDirecion() + "','" + v.getItemDelVendedor() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Vendedor  registrado exitosamente";
    }

    @Override
    public ArrayList<String> listarVendedor() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT identificacion, nombre FROM vendedor";
        vendedores = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Vendedor tmpVendedor = new Vendedor();
            tmpVendedor.setIdentificacion(rs.getString("identificacion"));
            tmpVendedor.setNombre(rs.getString("nombre"));
            tmpVendedor.setApellido(rs.getString("apellido"));
            tmpVendedor.setCorreo(rs.getString("correo"));
            tmpVendedor.setDirecion(rs.getString("direccion"));
            tmpVendedor.setPuntuacion(Double.parseDouble(rs.getString("puntuacion")));
            tmpVendedor.add(tmpVendedor);
        }
        return vendedores;
    }

    @Override
    public String eliminarVendedor(String identificacion) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM vendedor WHERE codigo= '" + identificacion + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " interes eliminado de manera correcta";
    }

    @Override
    public Vendedor buscarVendedor(String identificacion) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  vendedor WHERE identificacion ='" + identificacion + "'";
        ResultSet rsInteres = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsInteres.next()) {
            Vendedor v = new Vendedor(rsInteres.getString("identificacion"), rsInteres.getString("nombre"), rsInteres.getString("apellido"),rsInteres.getString("correo"),rsInteres.getString("direccion"),(rsInteres.getDouble("puntuacion")));
            return v;
        }
        return null;
    }

    @Override
    public String modificarVendedro(Vendedor vendedor) throws ClassNotFoundException, SQLException, Exception {
        query = "UPDATE vendedor SET nombre='" + vendedor.getNombre() + "'" +
                " WHERE identificacion= '" + vendedor.getIdentificacion() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Vendedor actualizado de manera correcta";
    }

}