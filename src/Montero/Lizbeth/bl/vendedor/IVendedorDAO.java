package Montero.Lizbeth.bl.vendedor;

import Montero.Lizbeth.bl.categoria.Categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IVendedorDAO {

    String insertarVendedor(Vendedor v) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listarVendedor() throws ClassNotFoundException,SQLException,Exception;
    String  eliminarVendedor(String identificacion) throws ClassNotFoundException,SQLException,Exception;;
    Vendedor buscarVendedor (String identificacion) throws ClassNotFoundException,SQLException,Exception;;
    String modificarVendedro(Vendedor vendedor ) throws ClassNotFoundException, SQLException, Exception ;
}
