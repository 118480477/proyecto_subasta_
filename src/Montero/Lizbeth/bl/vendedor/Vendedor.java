package Montero.Lizbeth.bl.vendedor;

import Montero.Lizbeth.bl.item.Item;

import java.util.ArrayList;

/**
 * Atributos de la clase Vendedor y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Vendedor {
    private String nombre, apellido, correo, direcion, identificacion;
    private double puntuacion;

    private ArrayList<Item> itemDelVendedor;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Vendedor() {
    }

    /**
     * Constructor con los atributos propios del Vendedor.
     *
     * @param nombre         tipo String, nombre del vendedor.
     * @param apellido       tipo String, apellido del vendedor.
     * @param correo         tipo String, correo del vendedor.
     * @param direcion       tipo String, dirrecion del vendedor.
     * @param identificacion tipo String, identificacion personal del vendedor.
     * @param puntuacion     tipo double, puntuacion del vendedor.
     */

    public Vendedor(String nombre, String apellido, String correo, String direcion, String identificacion, double puntuacion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direcion = direcion;
        this.identificacion = identificacion;
        this.puntuacion = puntuacion;
    }

    /**
     * Constructor con los atributos propios del Vendedor y su relacion.
     *
     * @param nombre            tipo String, nombre del vendedor.
     * @param apellido          tipo String, apellido del vendedor.
     * @param correo            tipo String, correo del vendedor.
     * @param direcion          tipo String, dirrecion del vendedor.
     * @param identificacion    tipo String, identificacion personal del vendedor.
     * @param puntuacion        tipo double, puntuacion del vendedor.
     * @param itemDelVendedor tipo ArrayList de   Item, donde se almacenan los objetos del vendedor.
     */
    public Vendedor(String nombre, String apellido, String correo, String direcion, String identificacion, double puntuacion, ArrayList<Item> itemDelVendedor) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direcion = direcion;
        this.identificacion = identificacion;
        this.puntuacion = puntuacion;
        this.itemDelVendedor = itemDelVendedor;
    }

    /**
     * Metodo Getter de la variable que almacena el itemDelVendedor del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el itemDelVendedor del Vendedor, lo hace publico.
     */
    public ArrayList<Item> getItemDelVendedor() {
        return itemDelVendedor;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param itemDelVendedor: dato que almacena el itemDelVendedor de manera privada en su calse respectiva.
     */
    public void setItemDelVendedor(ArrayList<Item> itemDelVendedor) {
        this.itemDelVendedor = itemDelVendedor;
    }

    /**
     * Metodo Getter de la variable que almacena el apellido del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el apellido del Vendedor, lo hace publico.
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param apellido: dato que almacena el apellido de manera privada en su calse respectiva.
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Metodo Getter de la variable que almacena la identificacion del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la identificacion del Vendedor, lo hace publico.
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena la identificacion  de manera privada en su calse respectiva.
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la puntuacion del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacion del Vendedor, lo hace publico.
     */
    public double getPuntuacion() {
        return puntuacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacion: dato que almacena la puntuacion  de manera privada en su calse respectiva.
     */
    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    /**
     * Metodo Getter de la variable que almacena el nombre del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del Vendedor, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre  de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena el correo del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el correo del Vendedor, lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena el correo  de manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Metodo Getter de la variable que almacena la direcion del Vendedor , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la direcion del Vendedor, lo hace publico.
     */
    public String getDirecion() {
        return direcion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param direcion: dato que almacena la direcion de manera privada en su calse respectiva.
     */
    public void setDirecion(String direcion) {
        this.direcion = direcion;
    }

    /**
     * metodo que se utiliza para agregar el item al vendedor.
     *
     * @param item tipo Item, item del que es duenno el vendedor.
     */
    public void agreagarObjeto(Item item) {
        itemDelVendedor.add(item);
    }

    /**
     * metodo que se usa para recorrer el ArrayList que contien los objetos del vendedor.
     *
     * @return datoObjeto devuelve los datos del ArrayList.
     */
    public String objetoToString() {
        String datoObjeto = "";
        for (Item item : itemDelVendedor) datoObjeto += item.toString();
        return datoObjeto;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del Vendedor ,con el respectivo valor de cada atributo en un solo String.
     */
    @Override
    public String toString() {
        return "Vendedor{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", correo='" + correo + '\'' +
                ", direcion='" + direcion + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", puntuacion=" + puntuacion +
                '}';
    }
    /**
     * metodo de identificacion de un objeto
     * @param o tipo object, el objeto el cual se compara o se busca
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendedor vendedor = (Vendedor) o;
        return getIdentificacion().equals(vendedor.getIdentificacion());
    }

    public void add(Vendedor tmpVendedor) {
    }
}
