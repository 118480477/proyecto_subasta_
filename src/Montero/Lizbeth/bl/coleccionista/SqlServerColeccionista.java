package Montero.Lizbeth.bl.coleccionista;


import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerColeccionista implements IColeccionistaDAO {
    private String query;
    private ArrayList<String> coleccionistas ;

    public SqlServerColeccionista() {
    }

    @Override
    public String registrarColeccionista(Coleccionista col) throws Exception {
        query = "INSERT INTO usuario (identificacion,tipo,nombre,avatar,fechanacimiento,edad,correo,contrasenia,estado,calificacion,dirreccion,esModerador,subasta_codigo) VALUES ('" +
               col.getIdentificacion()+ "','" +col.getTipo()+"','"+ col.getNombre() + "','"+ col.getAvatar()+"','"+col.getFechaNacimiento()+"','"
                +col.getEdad()+"','"+col.getCorreo()+"','"+col.getPassword()+"','"+col.getEstado()+"','"+col.getCalificacion()+"','"+col.getDireccion()+"','"
                +col.getModerador()+"','"+col.getIdSubasta()+"')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El coleccionista se registró exitosamnete";
    }

    @Override
    public String modificarColeccionista(Coleccionista  identificacion) throws Exception {
        query = "UPDATE CATEGORIA SET nombre='" + identificacion.getNombre()+ "'" +
                " WHERE codigo= '" + identificacion.getIdentificacion()+ "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Categoría actualizada de manera correcta";
    }

    @Override
    public String eliminarColeccionista(String identificacion) throws Exception {
        query = "DELETE FROM usuario WHERE codigo= '" + identificacion + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Coleccionista eliminada de manera correcta";
    }


    @Override
    public Coleccionista buscarColeccionista(String identificacion) throws Exception {
       query = "SELECT * FROM usuario WHERE identificacion ='" + identificacion + "'";
        ResultSet rsColeccionista = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsColeccionista.next()) {
            Coleccionista cole = new Coleccionista(rsColeccionista.getString("codigo"), rsColeccionista.getString("tipo"),rsColeccionista.getString("nombre"),rsColeccionista.getString("avatar"),
                    LocalDate.parse(rsColeccionista.getString("fechanacimiento")),rsColeccionista.getString("edad"),rsColeccionista.getString("correo"),rsColeccionista.getString("contrasenia"),rsColeccionista.getString("estado"),
                    Double.parseDouble(rsColeccionista.getString("calificacion")),rsColeccionista.getString("direccion"),rsColeccionista.getString("esModerador"),rsColeccionista.getString("subasta_codigo"));
            return cole;
        }
        return null;
    }
// TODO
    @Override
    public ArrayList<String> listarColeccionistas() throws Exception {
        return null;
    }

    @Override
    public String asociarInteresColeccionista(String idColeccionista, String codInteres) throws Exception {
        return null;
    }

    @Override
    public String asociarObjetoColeccionista(String idColeccionista, String codObjeto) throws Exception {
        query = "INSERT INTO ITEM_usuario(idcoleccionista, ITEM_codigo) VALUES ('" +idColeccionista + "','" + codObjeto + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El item con codigo "+ codObjeto + " se agregó de forma correcta al coleccionista";
    }
}
