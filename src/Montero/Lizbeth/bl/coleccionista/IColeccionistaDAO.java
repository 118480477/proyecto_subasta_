package Montero.Lizbeth.bl.coleccionista;

import Montero.Lizbeth.bl.usuario.Usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IColeccionistaDAO {
        public String registrarColeccionista(Coleccionista coleccionista) throws Exception;
        public String modificarColeccionista(Coleccionista identificacion) throws Exception;
        public String eliminarColeccionista(String identificacion) throws Exception;
        public Coleccionista buscarColeccionista(String identificacion) throws Exception;
        public ArrayList<String> listarColeccionistas() throws Exception;
        public String asociarInteresColeccionista(String idColeccionista, String codInteres) throws Exception;
        public String asociarObjetoColeccionista(String idColeccionista, String codObjeto) throws Exception;

    }

