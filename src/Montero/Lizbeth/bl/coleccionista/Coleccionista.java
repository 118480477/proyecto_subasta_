package Montero.Lizbeth.bl.coleccionista;

import Montero.Lizbeth.bl.interes.Interes;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.usuario.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Coleccionista
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */

public class Coleccionista extends Usuario {
    private int  edad;
    private String  avatar, estado, correo, direccion, moderador,identificacion,tipo,idSubasta;
    private double calificacion;
    private LocalDate fechaNacimiento;
    private Item itemDue;
    private ArrayList<Interes> intereses;
    private ArrayList<Item> objetos;

    /**
     *  contructor por defecto es utilizado para la invocación de las subclases, palabra reservada super que obtiene los metodos del padre
     */
    public Coleccionista() {
        super();
    }

    /**
     * Contructor con los atributos propios de la clase.
     *
     * @param nombre tipo string, guarda el nombre del coleccionista
     * @param username tipo string guarda el nombre de usuario del coleccionista
     * @param password tipo string, guarda la contrasennia del coleccionista
     * @param edad tipo int guarda la edad del coleccionista
     * @param avatar tipo String guarda del avatras del coleccionista
     * @param estado tipo String , guarda el estado del coleccionista (activo o no )
     * @param correo tipo String guarda el correo del coleccionista.
     * @param direccion tipo string guarda la direecion del coleccionista
     * @param moderador tipo String, guarda el dato si el coleccionista tambien es moderador o no
     * @param calificacion tipo double, guarda la calificacion del coleccionista
     * @param fechaNacimiento tipo localDate, guarda la fecha en la que nacio el coleccionista
     */

    public Coleccionista(String nombre, String username, String password, String identificacion, int edad, String avatar, String estado, String correo, String direccion, String moderador, double calificacion, LocalDate fechaNacimiento) {
    }

    /**
     *  Contructor con todos los parametros, incluyendo los del padre.
     * @param nombre tipo string, guarda el nombre del coleccionista
     * @param username tipo string guarda el nombre de usuario del coleccionista
     * @param password tipo string, guarda la contrasennia del coleccionista
     * @param edad tipo int guarda la edad del coleccionista
     * @param avatar tipo String guarda del avatras del coleccionista
     * @param estado tipo String , guarda el estado del coleccionista (activo o no )
     * @param correo tipo String guarda el correo del coleccionista.
     * @param direccion tipo string guarda la direecion del coleccionista
     * @param moderador tipo String, guarda el dato si el coleccionista tambien es moderador o no
     * @param identificacion tipo string guarda la cedula de l coleccionista.
     * @param calificacion tipo double, guarda la calificacion del coleccionista
     * @param fechaNacimiento tipo localDate, guarda la fecha en la que nacio el coleccionista
     * @param itemDue tipo item, guarda  si el coleccionista tiene objetos o no .
     * @param intereses arraylist de interes, guarda la serie de intereses del coleccionista.
     * @param objetos  arraylist de item, guarda todos los objetos del coleccionista
     */
    public Coleccionista(String nombre, String username, String password, int edad, String avatar, String estado, String correo, String direccion, String moderador, String identificacion, double calificacion, LocalDate fechaNacimiento, Item itemDue, ArrayList<Interes> intereses, ArrayList<Item> objetos) {
        super(nombre, username, password);
        this.edad = edad;
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.direccion = direccion;
        this.moderador = moderador;
        this.identificacion = identificacion;
        this.calificacion = calificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.itemDue = itemDue;
        this.intereses = intereses;
        this.objetos = objetos;
    }

    public Coleccionista(String codigo, String tipo, String nombre, String avatar, LocalDate fechanacimiento, String edad, String correo, String contrasenia, String estado, double calificacion, String direccion, String esModerador, String subasta_codigo) {
    }

    /**
     * Metodo Getter de la variable que almacena si es moderador, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve si es moderador y , lo hace publico.
     */
    public String getModerador() {
        return moderador;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param moderador: dato que almacena si es moderador o no  de manera privada en su calse respectiva.
     */
    public void moderador(String moderador) {
        this.moderador = moderador;
    }
    /**
     * Metodo Getter de la variable que almacena la identificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la identificacion y , lo hace publico.
     */
    public String getIdentificacion() {
        return identificacion;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena  la identificacion  manera privada en su calse respectiva.
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    /**
     * Metodo Getter de la variable que almacena la edad, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la edad del coleccionista y , lo hace publico.
     */
    public int getEdad() {
        return edad;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param edad: dato que almacena  la edad  manera privada en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    /**
     * Metodo Getter de la variable que almacena el nombre del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el nombre del coleccionista y , lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre  manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo Getter de la variable que almacena el avatar del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el avatar del coleccionista y , lo hace publico.
     */
    public String getAvatar() {
        return avatar;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param avatar: dato que almacena el avatar  manera privada en su calse respectiva.
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    /**
     * Metodo Getter de la variable que almacena el estado del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el estado del coleccionista y , lo hace publico.
     */
    public String getEstado() {
        return estado;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena el estado  manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    /**
     * Metodo Getter de la variable que almacena el correo del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el correo del coleccionista y , lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena el correo  manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    /**
     * Metodo Getter de la variable que almacena la direccion del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la direccion del coleccionista y , lo hace publico.
     */
    public String getDireccion() {
        return direccion;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param direccion: dato que almacena la direccion  manera privada en su calse respectiva.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    /**
     * Metodo Getter de la variable que almacena la contrasennia del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del coleccionista y , lo hace publico.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la password  manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * Metodo Getter de la variable que almacena la calificacion del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la calificacion del coleccionista y , lo hace publico.
     */
    public double getCalificacion() {
        return calificacion;
    }
    /**
     * Metodo Getter de la variable que almacena el item  del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el item del coleccionista y , lo hace publico.
     */
    public Item getItemDue() {
        return itemDue;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param itemDue: dato que almacena el itemDue  manera privada en su calse respectiva.
     */
    public void setItemDue(Item itemDue) {
        this.itemDue = itemDue;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param calificacion: dato que almacena la calificacion  manera privada en su calse respectiva.
     */
    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }
    /**
     * Metodo Getter de la variable que almacena la fecha de nacimiento   del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de nacimiento del coleccionista y , lo hace publico.
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaNacimiento: dato que almacena la fechaNacimiento  manera privada en su calse respectiva.
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setModerador(String moderador) {
        this.moderador = moderador;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<Interes> getIntereses() {
        return intereses;
    }

    public void setIntereses(ArrayList<Interes> intereses) {
        this.intereses = intereses;
    }

    public ArrayList<Item> getObjetos() {
        return objetos;
    }

    public void setObjetos(ArrayList<Item> objetos) {
        this.objetos = objetos;
    }

    public String getIdSubasta() {
        return idSubasta;
    }

    public void setIdSubasta(String idSubasta) {
        this.idSubasta = idSubasta;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del coleccionista, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */



    @Override
    public String toString() {
        return "Coleccionista{" +
                "identificacion=" + identificacion +
                ", edad=" + edad +
                ", nombre='" + nombre + '\'' +
                ", avatar='" + avatar + '\'' +
                ", estado='" + estado + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", password='" + password + '\'' +
                ", moderador='" + moderador + '\'' +
                ", calificacion=" + calificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                ", nombre='" + nombre + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
