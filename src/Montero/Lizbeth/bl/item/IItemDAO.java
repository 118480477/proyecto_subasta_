package Montero.Lizbeth.bl.item;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IItemDAO {
    String insertar(Item i) throws ClassNotFoundException, SQLException, Exception;

    ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception;

    String eliminarItem(String codigo) throws ClassNotFoundException, SQLException, Exception;

    String modificarItem(Item items) throws ClassNotFoundException, SQLException, Exception;

    String buscarItem(String codigo) throws ClassNotFoundException, SQLException, Exception;

}
