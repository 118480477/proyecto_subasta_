package Montero.Lizbeth.bl.item;

import java.time.LocalDate;

/**
 * Atributos de la clase Item.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Item {
    private  String codigo1;
    private String nombre, descripcion, estado, imagen, categoria,id,categoria_codigo,codigo;
    private LocalDate fechaCompra,fechaAntiguedad;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Item() {
    }

     /**
     * Constructor con todos los parametros.
     *
     * @param nombre          tipo String, nombre del objeto
     * @param descripcion     tipo String, descripcion del objeto
     * @param estado          tipo String, estado del obajeto, nouevo,viejo...
     * @param imagen          tipo String, imagen del objeto
     * @param categoria       tipo String,  categoria del objeto.
     * @param fechaAntiguedad tipo String, fecha de antiguedad del objeto
     * @param fechaCompra     tipo LocalDate, fecha en que se adquirio
     */
   /* public Item(String nombre, String descripcion, String estado, String imagen, String categoria, String fechaAntiguedad, LocalDate fechaCompra, String id) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagen = imagen;
        this.categoria = categoria;
        this.fechaAntiguedad = fechaAntiguedad;
        this.id = id;
        this.fechaCompra = fechaCompra; }

*/
    /**
     * Constructor con todos los parametros.
     *
     * @param nombre          tipo String, nombre del objeto
     * @param descripcion     tipo String, descripcion del objeto
     * @param estado          tipo String, estado del obajeto, nouevo,viejo...
     * @param imagen          tipo String, imagen del objeto
     * @param categoria       tipo String,  categoria del objeto.
     * @param fechaAntiguedad tipo String, fecha de antiguedad del objeto
     * @param fechaCompra     tipo LocalDate, fecha en que se adquirio
     */
   /* public Item(String nombre, String descripcion, String estado, String imagen, String categoria, LocalDate fechaAntiguedad, LocalDate fechaCompra, String codigo,String categoria_codigo) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagen = imagen;
        this.categoria = categoria;
        this.fechaAntiguedad = fechaAntiguedad;
        this.fechaCompra = fechaCompra;
        this.codigo = codigo;
        this.categoria_codigo = categoria_codigo;
    }*/

   /* public Item(String nombre, String descripcion, String estado, String imagen, String categoria, LocalDate fechaAntiguedad, LocalDate fechaCompra, String id) {
    }*/

    public Item( String codigo, String nombre,String descripcion, String estado, String imagen, LocalDate fechaCompra, LocalDate fechaAntiguedad,String id, String categoria_codigo) {

        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagen = imagen;
        this.id = id;
        this.categoria_codigo = categoria_codigo;
        this.codigo = codigo;
        this.codigo1 = codigo1;
        this.fechaCompra = fechaCompra;
        this.fechaAntiguedad = fechaAntiguedad;

    }



   /* public Item(String codigo, String nombre, String descripcion, String estado, String imagen, String fechaCompra, String antiguedad, String codigo1, String categoria_codigo) {
    }*/

    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el nombre del objeto, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena  el nombre del objeto de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena la descripcion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la descripcion del objeto, lo hace publico.
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param descripcion: dato que almacena  la descripcion del objeto de manera privada en su calse respectiva.
     */

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Metodo Getter de la variable que almacena el estado, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el estado, del objeto, lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena   el estado, del objeto de manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena la imagen , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la imagen, del objeto, lo hace publico.
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param imagen: dato que almacena  la imagen, del objeto de manera privada en su calse respectiva.
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * Metodo Getter de la variable que almacena la categoria , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la categoria, del objeto, lo hace publico.
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param categoria: dato que almacena  la categoria, del objeto de manera privada en su calse respectiva.
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * Metodo Getter de la variable que almacena la fechaAntiguedad , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la fechaAntiguedad, del objeto, lo hace publico.
     */
    public LocalDate getFechaAntiguedad() {
        return fechaAntiguedad;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaAntiguedad: dato que almacena  la fechaAntiguedad, del objeto de manera privada en su calse respectiva.
     */
    public void setFechaAntiguedad(LocalDate fechaAntiguedad) {
        this.fechaAntiguedad = fechaAntiguedad;
    }

    /**
     * Metodo Getter de la variable que almacena la fechaCompra , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la fechaCompra, del objeto, lo hace publico.
     */
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaCompra: dato que almacena  la fechaCompra, del objeto de manera privada en su calse respectiva.
     */
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoria_codigo() {
        return categoria_codigo;
    }

    public void setCategoria_codigo(String categoria_codigo) {
        this.categoria_codigo = categoria_codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public String setCodigo(String codigo) {
        this.codigo = codigo;
        return codigo;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto,con el respectivo valor de cada atributo en un solo String.
     */

    @Override
    public String toString() {
        return "Item{" +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", estado='" + estado + '\'' +
                ", imagen='" + imagen + '\'' +
                ", categoria='" + categoria + '\'' +
                ", fechaAntiguedad='" + fechaAntiguedad + '\'' +
                ", id='" + id + '\'' +
                ", fechaCompra=" + fechaCompra +
                '}';
    }

    public void add(Item tmpItem) {
    }
}//END CLASS
