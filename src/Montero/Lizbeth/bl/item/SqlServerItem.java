package Montero.Lizbeth.bl.item;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerItem implements IItemDAO {
    private String query;
    private ArrayList<String> items;

    //TODO REVISAR COMILLAS SIMPLES
    @Override
    public String insertar(Item i) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO ITEM (codigo,nombre,descripcion,estado,imagen,fechaCompra,antiguedad,codigo1,categoria_codigo) " +
                "VALUES" + " ('" + i.getId() + "','" + i.getNombre() + "','" + i.getDescripcion() + "','" + i.getEstado() + "','"
                + i.getImagen() + "','"
                + i.getFechaCompra() + "','" + i.getFechaAntiguedad() + "','" + i.getCodigo() + "','" + i.getCategoria_codigo() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "interes  registrado exitosamente";
    }

    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo,nombre,descripcion,estado,imagen,fechaCompra,antiguedad,codigo1,categoria_codigo FROM ITEM";
        items = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Item tmpItem = new Item();
            tmpItem.setId(rs.getString("codigo"));
            tmpItem.setNombre(rs.getString("nombre"));
            tmpItem.setDescripcion(rs.getString("descripcion"));
            tmpItem.setEstado(rs.getString("estado"));
            tmpItem.setImagen(rs.getString("imagen"));
            tmpItem.setFechaCompra(LocalDate.parse(rs.getString("fechaCompra")));
            tmpItem.setCodigo(rs.getString("codigo1"));
            tmpItem.setFechaAntiguedad(LocalDate.parse(rs.getString("antiguedad")));
            tmpItem.setCategoria_codigo(rs.getString("categoria_codigo"));
            tmpItem.add(tmpItem);
        }
        return items;
    }


    @Override
    public String eliminarItem(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM ITEM WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " Objeto eliminado de manera correcta";
    }


    @Override
    public String modificarItem(Item items) throws ClassNotFoundException, SQLException, Exception {
        query = "UPDATE ITEM SET nombre='" + items.getNombre() + "'" +
                " WHERE codigo= '" + items.getId() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Item actualizado de manera correcta";
    }




    @Override
    public String buscarItem(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  interes WHERE codigo ='" + codigo + "'";
        ResultSet rsItem = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsItem.next()) {
            Item it = new Item(rsItem.getString("codigo"), rsItem.getString("nombre"), rsItem.getString("descripcion"),
                    rsItem.getString("estado"), rsItem.getString("imagen"), LocalDate.parse(rsItem.getString("fechaCompra")), LocalDate.parse(rsItem.getString("antiguedad")),
                    rsItem.getString("codigo1"), rsItem.getString("categoria_codigo"));
            return it.toString();
        }
        return null;
    }
}
