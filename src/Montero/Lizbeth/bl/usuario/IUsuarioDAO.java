package Montero.Lizbeth.bl.usuario;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IUsuarioDAO {
    String insertarUsuario(Usuario u) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listarUsuario() throws ClassNotFoundException,SQLException,Exception;
    String eliminarUsuario(String userName)throws ClassNotFoundException,SQLException,Exception;;
    String eliminarpasword(String pasword)throws ClassNotFoundException,SQLException,Exception;;
    Usuario buscarUsuario (String userName) throws ClassNotFoundException,SQLException,Exception;;
    String modificarPasword(Usuario u) throws ClassNotFoundException, SQLException, Exception ;


}
