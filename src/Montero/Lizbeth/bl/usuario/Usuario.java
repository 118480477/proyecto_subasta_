package Montero.Lizbeth.bl.usuario;

import java.util.Objects;

/**
 * Atributos de la clase Categoria
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */
public  class Usuario {
    protected String nombre, username, password,identificacion;

    /**
     * Contructor por defecto, clase padre.
     */
    public Usuario() {
    }

    /**
     *
     * @param nombre tipo string guarda el nombre del usuario
     * @param username tipo string guarda el nombre de usuario del usuario.
     * @param password tipo string guarda la contrasennia del usuario
     */
    public Usuario(String identificacion,String nombre, String username, String password) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.username = username;
        this.password = password;
    }

    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del usuario, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre del usuario de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo Getter de la variable que almacena el username del usuario, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del usuario y , lo hace publico.
     */
    public String getUsername() {
        return username;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param username: dato que almacena el username  manera privada en su calse respectiva.
     */
    public void setUsername(String username) {
        this.username = username;
    }
    /**
     * Metodo Getter de la variable que almacena la contrasennia del usuario, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del usuario y , lo hace publico.
     */
    public String getPassword() {
        return password;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la password  manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del Usuario, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */


    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
    /**
     * metodo de identificacion de un objeto
     * @param o tipo object, el objeto el cual se compara o se busca
     * @return true o false
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return getUsername().equals(usuario.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }
}
