package Montero.Lizbeth.bl.usuario;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerUsuario implements  IUsuarioDAO {
    private String query;
    private ArrayList<String> usuarios;

    @Override
    public String insertarUsuario(Usuario u) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO usuario (identificacion,nombre, correo ,contrasenia ) VALUES ('" +
                u.getIdentificacion()+"','"+u.getNombre()+"','"+u.getUsername()+ "','" + u.getPassword() +"')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El usuario se registró exitosamnete";
    }


    @Override
    public ArrayList<String> listarUsuario() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo, nombre FROM usuario";
        usuarios = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Usuario u = new Usuario();
            u.setIdentificacion(rs.getString("identificacion"));
            u.setNombre(rs.getString("nombre"));
            u.setUsername(rs.getString("correo"));
            u.setUsername(rs.getString("contrasenia"));
            usuarios.add(u.toString());
        }
        return usuarios;
    }

    @Override
    public String eliminarUsuario(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM usuario WHERE correo= '" + userName + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Categoría eliminada de manera correcta";
    }

    @Override
    public String eliminarpasword(String pasword) throws ClassNotFoundException, SQLException, Exception {
        return null;
    }

    @Override
    public Usuario buscarUsuario(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM usuario WHERE correo ='" + userName + "'";
        ResultSet rsUsuario = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsUsuario.next()) {
            Usuario u = new Usuario(rsUsuario.getString("identificacion"), rsUsuario.getString("nombre"), rsUsuario.getString("correo"), rsUsuario.getString("contrasenia"));
            return u;
        }
        return null;
    }

    @Override
    public String modificarPasword(Usuario u) throws ClassNotFoundException, SQLException, Exception {
        return null;
    }
}
