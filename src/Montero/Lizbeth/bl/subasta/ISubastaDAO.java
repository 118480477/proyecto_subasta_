package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.bl.interes.Interes;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ISubastaDAO {

    String insertarSubasta(Subasta s) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listarSubasta() throws ClassNotFoundException,SQLException,Exception;
    String eliminarSubasta(String codigo) throws ClassNotFoundException,SQLException,Exception;;
    Subasta buscarSubasta (String codigo) throws ClassNotFoundException,SQLException,Exception;;
    String modificarSubasta(Subasta subasta) throws ClassNotFoundException, SQLException, Exception ;

}
