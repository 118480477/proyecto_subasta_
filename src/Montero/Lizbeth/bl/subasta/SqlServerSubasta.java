package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.interes.Interes;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;


public class SqlServerSubasta implements ISubastaDAO{

    private String query;
    private ArrayList<String> subastas;

    public SqlServerSubasta() {
    }


    public String insertarSubasta(Subasta s) throws ClassNotFoundException, SQLException, Exception {
       query = "INSERT INTO subasta (codigo, fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, horaVencimiento, estado, precioBase, ordencompra_codigo) VALUES ('" +s.getCodigo()+"','"+
                "'" + s.getFechaCreacion() + "','" + s.getFechaInicio() +
                "'," + s.getFechaVencimiento() + "','" + s.getHoraVencimiento() + "','" + s.setEstadoSubasta() + "','" + s.getPrecioMinimoInicial()+"','" +s.getOrdenCompra_codigo()+ "')";
        Conector.getConector(Utils.getProperties()[0],Utils.getProperties()[1]).ejecutarQuery(query);
        return "Subasta registrada exitosamente";
    }

  public ArrayList<String> listarSubasta() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, horaVencimiento, estado, precioBase FROM subasta";
        subastas = new ArrayList<>();
        ResultSet rs = null;
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Subasta tmpSubasta = new Subasta();
            tmpSubasta.setCodigo(rs.getString("codigo"));
            tmpSubasta.setFechaCreacion(LocalDate.parse(rs.getString("fechaCreacion")));
            tmpSubasta.setFechaInicio(LocalDate.parse(rs.getString("fechaInicio")));
            tmpSubasta.setHoraInicio(rs.getString("horaInicio"));
            tmpSubasta.setFechaVencimiento(LocalDate.parse(rs.getString("fechaVencimiento ")));
            tmpSubasta.setEstadoSubasta(rs.getString("estado"));
            tmpSubasta.setPrecioMinimoInicial(Double.parseDouble(rs.getString("fechaInicio")));
            tmpSubasta.setOrdenCompra_codigo(rs.getString("ordencompra_codigo"));
            tmpSubasta.add(tmpSubasta);
        }
        return subastas;
    }

    @Override
    public String eliminarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM subasta WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " Subasta eliminada de manera correcta";
    }

    @Override
    public Subasta buscarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  subasta WHERE codigo ='" + codigo + "'";
        ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsSubasta.next()) {
            Subasta su = new Subasta(rsSubasta.getString("codigo"),LocalDate.parse(rsSubasta.getString("fechaCreacion")) ,
                    rsSubasta.getString("horaInicio"), LocalDate.parse(rsSubasta.getString("horaVencimiento")),
                    rsSubasta.getString("estado"),rsSubasta.getString("precioBase"),
                    rsSubasta.getString("ordencompra_codigo"));
            return su;
        }
        return null;
    }

    @Override
    public String modificarSubasta(Subasta subasta) throws ClassNotFoundException, SQLException, Exception {
        query="UPDATE subasta SET nombre='"+subasta.getEstadoSubasta()+"'"+
                " WHERE codigo= '" + subasta.getCodigo()+"'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Interes actualizado de manera correcta";
    }

}
