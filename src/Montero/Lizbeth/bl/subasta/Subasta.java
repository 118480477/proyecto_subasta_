package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.oferta.Oferta;

import java.time.LocalDate;
import java.util.ArrayList;


/**
 * Atributos de la clase Subasta y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */

public class Subasta {
    private String codigo;
    private LocalDate fechaCreacion, fechaInicio, FechaVencimiento;
    private int horaInicio,horaVencimiento;
    private Item objetosAVender;
    private double puntuacionPromedio, precioMinimoInicial;
    private String estadoSubasta, nombreVendedor, nombreDelColeccionista, ordenCompra_codigo;
    private ArrayList<Oferta> ofertas;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */

    public Subasta() {
    }

    /**
     * Constructor con los atributos propios de la clase subasta.
     *  @param codigo                 tipo int, codigo de la subasta.
     * @param fechaCreacion          tipo LocalDate, fecha en que se crea la subasta.
     * @param fechaInicio            tipo LocalDate, fecha en la que se inicia la subasta.
     * @param fechaVencimiento       tipo LocalDate, fecha en la que se termina la subasta.
     * @param horaInicio             tipo int, hora en la que inicia la subasta.
     * @param objetosAVender         tipo Obejto, objeto que se va a subastar.
     * @param puntuacionPromedio     tipo double, la puntuacion de le dan a la subasta.
     * @param precioMinimoInicial    tipo double, precio base con el que se inicia la subasta.
     * @param estadoSubasta          tipo String, estado en el que se encuentra la subasta, abierta, cerrada.
     * @param nombreVendedor         tipo String, nombre del vendedor de los articulos que se encuentran el la subasta.
     * @param nombreDelColeccionista tipo String nombre del coleccionista que esta en la subasta.
     */
    public Subasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, LocalDate fechaVencimiento, int horaInicio, Item objetosAVender, double puntuacionPromedio, double precioMinimoInicial, String estadoSubasta, String nombreVendedor, String nombreDelColeccionista) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        FechaVencimiento = fechaVencimiento;
        this.horaInicio = horaInicio;
        this.objetosAVender = objetosAVender;
        this.puntuacionPromedio = puntuacionPromedio;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.nombreVendedor = nombreVendedor;
        this.nombreDelColeccionista = nombreDelColeccionista;
    }

    /**
     * Constructor con los atributos propios de la clase subasta y sus respectivas relaciones.
     *  @param codigo                 tipo int, codigo de la subasta.
     * @param fechaCreacion          tipo LocalDate, fecha en que se crea la subasta.
     * @param fechaInicio            tipo LocalDate, fecha en la que se inicia la subasta.
     * @param fechaVencimiento       tipo LocalDate, fecha en la que se termina la subasta.
     * @param horaInicio             tipo int, hora en la que inicia la subasta.
     * @param objetosAVender         tipo Obejto, objeto que se va a subastar.
     * @param puntuacionPromedio     tipo double, la puntuacion de le dan a la subasta.
     * @param precioMinimoInicial    tipo double, precio base con el que se inicia la subasta.
     * @param estadoSubasta          tipo String, estado en el que se encuentra la subasta, abierta, cerrada.
     * @param nombreVendedor         tipo String, nombre del vendedor de los articulos que se encuentran el la subasta.
     * @param nombreDelColeccionista tipo String nombre del coleccionista que esta en la subasta.
     * @param ofertas                tipo ArrayList se guardan las ofertas que se le realizan a un item.
     */
    public Subasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, LocalDate fechaVencimiento, int horaInicio, Item objetosAVender, double puntuacionPromedio, double precioMinimoInicial, String estadoSubasta, String nombreVendedor, String nombreDelColeccionista, ArrayList<Oferta> ofertas) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        FechaVencimiento = fechaVencimiento;
        this.horaInicio = horaInicio;
        this.objetosAVender = objetosAVender;
        this.puntuacionPromedio = puntuacionPromedio;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.nombreVendedor = nombreVendedor;
        this.nombreDelColeccionista = nombreDelColeccionista;
        this.ofertas = ofertas;
    }

    public Subasta(String codigo, int horaInicio, int horaVencimiento, LocalDate fechaCreacion, LocalDate fechaInicio, LocalDate fechaVencimiento, String estadoSubasta, double precioMinimoInicial,String ordenCompra_codigo) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        FechaVencimiento = fechaVencimiento;
        this.horaInicio = horaInicio;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.ordenCompra_codigo = ordenCompra_codigo;
    }

    public Subasta(String codigo, LocalDate fechaCreacion, String nombre, LocalDate horaVencimiento, String estado, String precioBase, String ordencompra_codigo) {
    }

    public Subasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, int horaInicio, LocalDate fechaVencimiento, int horaVencimiento, String estado, double precioBase, String ordencompra_codigo) {
    }


    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la subasta, lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo : dato que almacena el codigo de la subasta manera privada en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Creacion de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Creacion de la subasta, lo hace publico.
     */
    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaCreacion : dato que almacena la fecha de Creacion de la subasta manera privada en su calse respectiva.
     */
    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Inicio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Inicio de la subasta, lo hace publico.
     */
    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaInicio: dato que almacena la fecha de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena la Fecha de Vencimientode la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la Fecha de Vencimiento de la subasta, lo hace publico.
     */
    public LocalDate getFechaVencimiento() {
        return FechaVencimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaVencimiento: dato que almacena la e fecha de Vencimiento de la subasta manera privada en su calse respectiva.
     */
    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        FechaVencimiento = fechaVencimiento;
    }

    /**
     * Metodo Getter de la variable que almacena la hora de Inicio la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la horaInicio de la subasta, lo hace publico.
     */
    public int getHoraInicio() {
        return horaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaInicio: dato que almacena la hora de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena  los objetosAVender la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve los objetosAVender de la subasta, lo hace publico.
     */
    public Item getObjetosAVender() {
        return objetosAVender;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param objetosAVender: dato que almacena los objetosAVender de la subasta manera privada en su calse respectiva.
     */
    public void setObjetosAVender(Item objetosAVender) {
        this.objetosAVender = objetosAVender;
    }

    /**
     * Metodo Getter de la variable que almacena  la puntuacionPromedio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacionPromedio de la subasta, lo hace publico.
     */
    public double getPuntuacionPromedio() {
        return puntuacionPromedio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacionPromedio: dato que almacena la puntuacionPromedio de la subasta manera privada en su calse respectiva.
     */
    public void setPuntuacionPromedio(double puntuacionPromedio) {
        this.puntuacionPromedio = puntuacionPromedio;
    }

    /**
     * Metodo Getter de la variable que almacena el precioMinimoInicial de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precioMinimoInicial de la subasta, lo hace publico.
     */
    public double getPrecioMinimoInicial() {
        return precioMinimoInicial;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioMinimoInicial: dato que almacena el precioMinimoInicial de la subasta manera privada en su calse respectiva.
     */
    public void setPrecioMinimoInicial(double precioMinimoInicial) {
        this.precioMinimoInicial = precioMinimoInicial;
    }

    /**
     * Metodo Getter de la variable que almacena el estadoSubasta de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estadoSubasta de la subasta, lo hace publico.
     */
    public String getEstadoSubasta() {
        return estadoSubasta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estadoSubasta: dato que almacena el estadoSubasta de la subasta manera privada en su calse respectiva.
     */
    public void setEstadoSubasta(String estadoSubasta) {
        this.estadoSubasta = estadoSubasta;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreVendedor de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreVendedor de la subasta, lo hace publico.
     */
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreVendedor: dato que almacena el nombreVendedor de la subasta manera privada en su calse respectiva.
     */
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreDelColeccionista de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreDelColeccionista de la subasta, lo hace publico.
     */
    public String getNombreDelColeccionista() {
        return nombreDelColeccionista;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreDelColeccionista: dato que almacena el nombreDelColeccionista de la subasta manera privada en su calse respectiva.
     */
    public void setNombreDelColeccionista(String nombreDelColeccionista) {
        this.nombreDelColeccionista = nombreDelColeccionista;
    }

    /**
     * Metodo Getter de la variable que almacena las ofertas de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las ofertas de la subasta, lo hace publico.
     */
    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ofertas: dato que almacena las ofertas de la subasta manera privada en su calse respectiva.
     */
    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public int getHoraVencimiento() {
        return horaVencimiento;
    }

    public void setHoraVencimiento(int horaVencimiento) {
        this.horaVencimiento = horaVencimiento;
    }

    /**
     * metodo utilizado para agregar las ofertas a la subasta.
     *
     * @param idOferente         tipo int, codigo de oferta.
     * @param puntuacionOferente tipo double, la puntuacion del coleccionista.
     * @param precioOfertado     tipo double, precio que le ofrecen a un articulo.
     * @param fechaOferta        tipo LocalDate, fecha en que se hace la oferta
     * @param coleccionistas     tipo Coleccionista, coleccionista que hace la oferta.
     * @param itemSubastado    tipo Item, objeto al que se le está haciendo la oferta.
     */
    public void agregarOfertas(int idOferente, double puntuacionOferente, double precioOfertado, Item itemSubastado, LocalDate fechaOferta, Coleccionista coleccionistas) {
        Oferta oferta = new Oferta(idOferente, precioOfertado, fechaOferta, itemSubastado, coleccionistas, puntuacionOferente);
    }

    public String getOrdenCompra_codigo() {
        return ordenCompra_codigo;
    }

    public void setOrdenCompra_codigo(String ordenCompra_codigo) {
        this.ordenCompra_codigo = ordenCompra_codigo;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la subasta ,con el respectivo valor de cada atributo en un solo String.
     */
    @Override
    public String toString() {
        return "Subasta{" +
                "codigo=" + codigo +
                ", fechaCreacion=" + fechaCreacion +
                ", fechaInicio=" + fechaInicio +
                ", FechaVencimiento=" + FechaVencimiento +
                ", horaInicio=" + horaInicio +
                ", objetosAVender=" + objetosAVender +
                ", puntuacionPromedio=" + puntuacionPromedio +
                ", precioMinimoInicial=" + precioMinimoInicial +
                ", estadoSubasta='" + estadoSubasta + '\'' +
                ", nombreVendedor='" + nombreVendedor + '\'' +
                ", nombreDelColeccionista='" + nombreDelColeccionista + '\'' +
                ", ofertas=" + ofertas +
                '}';
    }

    public String setEstadoSubasta() {
        return null;
    }

    public void add(Subasta tmpSubasta) {
    }

    public void setHoraInicio(String fechaInicio) {
    }
}
