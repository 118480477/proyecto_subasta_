package Montero.Lizbeth.bl.categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICategoriaDAO {



    String insertar(Categoria c) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;
    String eliminar(String codigo)throws ClassNotFoundException,SQLException,Exception;;
    Categoria buscarCategoria (String codigo) throws Exception;;
     String modificarCategoria(Categoria categoria) throws ClassNotFoundException, SQLException, Exception ;


    }
