package Montero.Lizbeth.bl.categoria;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerCategoria implements ICategoriaDAO {
    private String query;
    private ArrayList<String> categorias;

    @Override
    public String insertar(Categoria c) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO categoria (codigo,nombre) VALUES ('" +
                c.getCodigo() + "','" + c.getNombre() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "La categoria se registró exitosamnete";
    }


    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo, nombre FROM categoria";
        categorias = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Categoria tmpCategoria = new Categoria();
            tmpCategoria.setCodigo(rs.getString("codigo"));
            tmpCategoria.setNombre(rs.getString("nombre"));
            categorias.add(tmpCategoria.toString());
        }
        return categorias;
    }

    @Override
    public String modificarCategoria(Categoria categoria) throws
            ClassNotFoundException, SQLException, Exception {
        query = "UPDATE CATEGORIA SET nombre='" + categoria.getNombre() + "'" +
                " WHERE codigo= '" + categoria.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Categoría actualizada de manera correcta";
    }

    @Override
    public String eliminar(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM categoria WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Categoría eliminada de manera correcta";
    }

    @Override
    public Categoria buscarCategoria(String codigo) throws Exception {

        query = "SELECT * FROM categoria WHERE codigo ='" + codigo + "'";
        ResultSet rsCategoria = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsCategoria.next()) {
            Categoria categoria = new Categoria(rsCategoria.getString("codigo"), rsCategoria.getString("nombre"));
            return categoria;
        }
        return null;
    }
}

