package Montero.Lizbeth.bl.interes;

import Montero.Lizbeth.bl.categoria.Categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IInteresDAO {

    String insertar(Interes in) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;
    String eliminar(String codigo) throws ClassNotFoundException,SQLException,Exception;;
    Interes buscarInteres (String codigo) throws ClassNotFoundException,SQLException,Exception;;
    String modificarInteres(Interes interes) throws ClassNotFoundException, SQLException, Exception ;
}
