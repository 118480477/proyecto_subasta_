package Montero.Lizbeth.bl.interes;

import java.util.Objects;

public class Interes {
    /**
     * Atributos de la clase Interes.
     *  @author Lizbeth Montero
     * @version 1.0.0
     * @since 1.0.0
     */
    private String codigoInteres;
    private String nombre,descripcion, usuario_id;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Interes(){}

    /**
     * contructor con los parametros propios de la clase interes.
     * @param codigoInteres tipo int, codigo de interes.
     * @param nombre tipo String nombre del interes.
     * @param descripcion tipo String descripcion del interes.
     */

    public Interes(String codigoInteres, String nombre, String descripcion) {
        this.codigoInteres = codigoInteres;
        this.nombre = nombre;
        this.descripcion = descripcion;

    }

    /**
     *Metodo Getter de la variable que almacena el codigoInteres, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el codigoInteres del interes, lo hace publico.
     */
    public String getCodigoInteres() {
        return codigoInteres;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param codigoInteres: dato que almacena el codigoInteres  de manera privada en su calse respectiva.
     */
    public void setCodigoInteres(String codigoInteres) {
        this.codigoInteres = codigoInteres;
    }

    /**
     *Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el nombre del interes, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param nombre: dato que almacena el nombre del interes  de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     *Metodo Getter de la variable que almacena la descripcion, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve la descripcion del interes, lo hace publico.
     */
    public String getDescripcion() {
        return descripcion;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param descripcion: dato que almacena la descripcion del interes  de manera privada en su calse respectiva.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos de los intereses, en este caso el respectivo valor de cada atributo de los intereses   en un solo String.
     */

    @Override
    public String toString() {
        return "Interes{" +
                "codigoInteres=" + codigoInteres +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }

    /**
     * metodo de identificacion de un objeto
     * @param o tipo object, el objeto el cual se compara o se busca
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interes interes = (Interes) o;
        return getCodigoInteres() == interes.getCodigoInteres() &&
                getNombre().equals(interes.getNombre()) &&
                Objects.equals(getDescripcion(), interes.getDescripcion());
    }

    public void add(Interes tmpInteres) {
    }
}
