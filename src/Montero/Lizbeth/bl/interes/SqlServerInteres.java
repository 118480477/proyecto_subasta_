package Montero.Lizbeth.bl.interes;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerInteres implements IInteresDAO {
    private String query;
    private ArrayList<String> intereses;

    @Override
    public String insertar(Interes in) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO interes (codigo,nombre,descripcion) VALUES ('" +
                in.getCodigoInteres() + "','" + in.getNombre() + "','"+in.getDescripcion()+"')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "interes  registrado exitosamente";

    }

    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo, nombre FROM interes";
        intereses = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Interes tmpInteres = new Interes();
            tmpInteres.setCodigoInteres(rs.getString("codigo"));
            tmpInteres.setNombre(rs.getString("nombre"));
            tmpInteres.setNombre(rs.getString("nombre"));
            tmpInteres.add(tmpInteres);
        }
        return intereses;
    }

    @Override
    public String eliminar(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM interes WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " interes eliminado de manera correcta";
    }

    @Override
    public Interes buscarInteres(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  interes WHERE codigo ='" + codigo + "'";
        ResultSet rsInteres = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsInteres.next()) {
            Interes inte = new Interes(rsInteres.getString("codigo"), rsInteres.getString("nombre"),rsInteres.getString("descripcion"));
            return inte;
        }
        return null;
    }


    public String modificarInteres(Interes interes) throws ClassNotFoundException ,SQLException,Exception {
        query="UPDATE interes SET nombre='"+interes.getNombre()+"'"+
                " WHERE codigo= '" + interes.getCodigoInteres()+"'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Interes actualizado de manera correcta";
    }

}

