package Montero.Lizbeth.bl.ordenDeCompra;

import Montero.Lizbeth.bl.categoria.Categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IOrdenDeCompraDAO {

    String insertar(OrdenDeCompra or) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listarOrden() throws ClassNotFoundException,SQLException,Exception;
    String eliminarOrden (String codigo)throws ClassNotFoundException,SQLException,Exception;
    OrdenDeCompra buscarOrden (String codigo) throws ClassNotFoundException,SQLException,Exception;
    String modificarOrden(OrdenDeCompra Orden) throws ClassNotFoundException, SQLException, Exception ;
}
