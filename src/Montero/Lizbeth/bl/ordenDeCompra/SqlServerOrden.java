package Montero.Lizbeth.bl.ordenDeCompra;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerOrden implements IOrdenDeCompraDAO {

    private String query;
    private ArrayList<String> ordenes;


    public SqlServerOrden() {
    }

    public String insertar(OrdenDeCompra or) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO ordencompra (codigo,fecha,precioTotal,usuario_identififcacion) VALUES ('"
                + or.getCodigo() + "','" + or.getFechaDeOrden() +
                "'," + or.getPrecioTotal() + "','" + or.getId_comprador() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Orden de compra registrada exitosamente";
    }

    @Override
    public String eliminarOrden(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM ordencompra WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "orden de compra  eliminada de manera correcta";
    }

    //TODO PROBLEMA DE FECHA
    @Override
    public OrdenDeCompra buscarOrden(String codigo) throws ClassNotFoundException, SQLException, Exception {
       /* query = "SELECT * FROM ordencompra WHERE codigo ='" + codigo + "'";
        ResultSet rsOrden = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsOrden.next()) {
            OrdenDeCompra orden = new OrdenDeCompra(rsOrden.getString("codigo"), rsOrden.getString("fecha"), rsOrden.getString("precioTotal"),rsOrden.getString("usuario_identififcacion")  );
            return orden;
        }*/
        return null;
    }

    @Override
    public String modificarOrden(OrdenDeCompra Orden) throws ClassNotFoundException, SQLException, Exception {
        query="UPDATE ordencoompra SET precioTotal='"+Orden.getPrecioTotal()+"'"+
                " WHERE codigo= '" + Orden.getCodigo()+"'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Orden de compra  actualizada de manera correcta";
    }

    public ArrayList<String> listarOrden() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo,fecha,precioTotal,usuario_identififcacion FROM ordencompra";
        ordenes = new ArrayList<>();
        ResultSet rs = null;
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            OrdenDeCompra tmpOrden = new OrdenDeCompra();
            tmpOrden.setCodigo(rs.getString("codigo"));
            tmpOrden.setFechaDeOrden(LocalDate.parse(rs.getString("fecha")));
            tmpOrden.setPrecioTotal(Double.parseDouble(rs.getString("precioTotal")));
            tmpOrden.setId_comprador(rs.getString("usuario_identififcacion"));
            tmpOrden.add(tmpOrden);
        }
        return ordenes;
    }
}

