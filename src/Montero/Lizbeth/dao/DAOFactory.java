package Montero.Lizbeth.dao;

import Montero.Lizbeth.bl.administrador.IAdministradorDAO;
import Montero.Lizbeth.bl.categoria.ICategoriaDAO;
import Montero.Lizbeth.bl.coleccionista.IColeccionistaDAO;
import Montero.Lizbeth.bl.interes.IInteresDAO;
import Montero.Lizbeth.bl.item.IItemDAO;
import Montero.Lizbeth.bl.oferta.IOfertaDAO;
import Montero.Lizbeth.bl.ordenDeCompra.IOrdenDeCompraDAO;
import Montero.Lizbeth.bl.subasta.ISubastaDAO;
import Montero.Lizbeth.bl.usuario.IUsuarioDAO;
import Montero.Lizbeth.bl.vendedor.IVendedorDAO;

public abstract class DAOFactory {

// SE DECLARAN LAS CONSTATES DE FORMA STATICA PARA  PARA TENER ACCSESO A ELLAS Y PODER DEFINIR EL TIPO DE MOTOR QUE SE VA USAR.
// NOS DICEN CUALES OBJETOS DE BASES DE DATROS VOY A NECESITAR

    public static final int SQLSERVER = 0;


// METODO  TIPO DAO FACTORI  RECIBE UN ENTERO Y  RETORNA EL VALOR DEL MOTO QUE SE VALLA A UTILIZAR

    //TODO PREGUNTAR AL PROFE
    public static DAOFactory getDAOFactory(int whichFactory) {
        switch (whichFactory) {
            case SQLSERVER:
                return new SqlServerDAOFactory();
            default:
                return null;
        }
    }




    // METODO ABSTRACTO QUE DEVUELVE UN OBJETO DEL TIPO DE METODO DE LA INTERFACE
    // le asigna la responsabilidad a las clasess que herendan de ella

    public abstract IAdministradorDAO getAdministrador();

    public abstract IColeccionistaDAO getColeccionista();

    public abstract ICategoriaDAO getCategoria();

    public abstract IInteresDAO getInteres();

    public abstract IItemDAO getItem();

    public abstract IOfertaDAO getOferta();

    public abstract IOrdenDeCompraDAO getOrdenDeCompra();

    public abstract ISubastaDAO getSubasta();

    public abstract IUsuarioDAO getUsuario();

    public abstract IVendedorDAO getVendedor();


}
