package Montero.Lizbeth.dao;

import Montero.Lizbeth.bl.administrador.IAdministradorDAO;
import Montero.Lizbeth.bl.administrador.SqlServerAdministrador;
import Montero.Lizbeth.bl.categoria.ICategoriaDAO;
import Montero.Lizbeth.bl.categoria.SqlServerCategoria;
import Montero.Lizbeth.bl.coleccionista.IColeccionistaDAO;
import Montero.Lizbeth.bl.coleccionista.SqlServerColeccionista;
import Montero.Lizbeth.bl.interes.IInteresDAO;
import Montero.Lizbeth.bl.interes.SqlServerInteres;
import Montero.Lizbeth.bl.item.IItemDAO;
import Montero.Lizbeth.bl.item.SqlServerItem;
import Montero.Lizbeth.bl.oferta.IOfertaDAO;
import Montero.Lizbeth.bl.oferta.SqlServerOferta;
import Montero.Lizbeth.bl.ordenDeCompra.IOrdenDeCompraDAO;
import Montero.Lizbeth.bl.ordenDeCompra.SqlServerOrden;
import Montero.Lizbeth.bl.subasta.ISubastaDAO;
import Montero.Lizbeth.bl.subasta.SqlServerSubasta;
import Montero.Lizbeth.bl.usuario.IUsuarioDAO;
import Montero.Lizbeth.bl.usuario.SqlServerUsuario;
import Montero.Lizbeth.bl.vendedor.IVendedorDAO;
import Montero.Lizbeth.bl.vendedor.SqlServerVendedor;

import java.sql.SQLException;

public class SqlServerDAOFactory extends DAOFactory {

    @Override

    public IAdministradorDAO getAdministrador() {
        //TODO PREGUNTAR AL PROFE EL COMPORTAMIENTO
        return new SqlServerAdministrador();
    }

    @Override
    public ISubastaDAO getSubasta() {
        return new SqlServerSubasta();
    }

    @Override
    public IColeccionistaDAO getColeccionista() {
        return new SqlServerColeccionista();
    }

    @Override
    public ICategoriaDAO getCategoria() {
        return new SqlServerCategoria();
    }

    @Override
    public IInteresDAO getInteres() {
        return new SqlServerInteres();
    }

    @Override
    public IItemDAO getItem() {
        return new SqlServerItem();
    }

    @Override
    public IOfertaDAO getOferta() {
        return new SqlServerOferta();
    }

    @Override
    public IOrdenDeCompraDAO getOrdenDeCompra() {
        return new SqlServerOrden();
    }


    @Override
    public IUsuarioDAO getUsuario() {
        return new SqlServerUsuario();
    }

    @Override
    public IVendedorDAO getVendedor() {
        return new SqlServerVendedor();
    }

}

