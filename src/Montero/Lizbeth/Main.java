package Montero.Lizbeth;

import Montero.Lizbeth.bl.categoria.Categoria;

import Montero.Lizbeth.tl.ContollerUsuario;
import Montero.Lizbeth.tl.ControllerCategoria;
import Montero.Lizbeth.tl.ControllerItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.SQLException;
import java.time.LocalDate;


class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ControllerCategoria gestorCategoria;
    static ContollerUsuario gestorUsuario= new  ContollerUsuario();


    static {
        try {
            gestorCategoria = new ControllerCategoria();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ControllerItem gestorI;

    static {
        try {
            gestorI = new ControllerItem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        try {
            //insertaritem();
         //  buscarItem();
           // modificarCategoria();
           // buscarCategoria();
           eliminarCategoria();
            // insertaritem();
            //insertar();
            //  listar();
            // buscarCategoria();
            //listaritem();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void eliminarCategoria() throws Exception {
        out.println("ingrese el codigo  ");
        String codigo = in.readLine();
        try {
            String resultado = gestorCategoria.eliminarCategoria(codigo);
            out.println(resultado);
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }



    public static void modificarCategoria() throws Exception {
        out.println("ingrese el codigo  ");
        String codigo = in.readLine();
        out.println("ingrese el nombre  ");
        String nombre = in.readLine();
        try {
            String resultado = gestorCategoria.modificarCategoria(codigo,nombre);
            out.println(resultado);
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }

    public static void buscarCategoria() throws Exception {
        out.println("ingrese el codigo  ");
        String codigo = in.readLine();
        out.println("ingrese el nombre  ");
        String nombre = in.readLine();
        try {
            String i = gestorCategoria.buscarCategoria(codigo);
            out.println(i);
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }

    public static void buscarItem() throws Exception {
        out.println("ingrese el codigo  ");
        String codigo = in.readLine();
        try {
            String i = gestorI.buscarItem(codigo);
            out.println(i);
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }





    public static void insertaritem() throws IOException {
        String codigo, nombre, estado, imagen, codigo1, categoria_codigo, descripcion;
        LocalDate fechaCompra, Antiguedad;
        int d1, m1, y1, d2, m2, y2;
        System.out.println("Ingrese el codigo");
        codigo = in.readLine();
        System.out.println("Ingrese el nombre del objeto");
        nombre = in.readLine();
        System.out.println("Ingrese la descripcion");
        descripcion = in.readLine();

        System.out.println("Ingrese el estado");
        estado = in.readLine();
        System.out.println("Ingrese la imagen");
        imagen = in.readLine();

        System.out.println("Ingrese  dia fecha de compra");
        d1 = Integer.parseInt(in.readLine());
        System.out.println("Ingrese  mes fecha de compra");
        m1 = Integer.parseInt(in.readLine());
        System.out.println("Ingrese  año fecha de compra");
        y1 = Integer.parseInt(in.readLine());
        fechaCompra = LocalDate.of(y1, m1, d1);
        // TODO ELIMIR (CODIGO1 DE LA BASE Y DE TODO LUGAR)
        System.out.println("Ingrese el codigo1 ");
        codigo1 = in.readLine();
        System.out.println("Ingrese el codigo categoria  ");
        categoria_codigo = in.readLine();

        System.out.println("Ingrese el dia de la fecha de antiguedad");
        d2 = Integer.parseInt(in.readLine());
        System.out.println("Ingrese el mes de la fecha de antiguedad");
        m2 = Integer.parseInt(in.readLine());
        System.out.println("Ingrese el año de la fecha de antiguedad");
        y2 = Integer.parseInt(in.readLine());
        Antiguedad = LocalDate.of(y2, m2, d2);

        out.println(gestorI.registrarItem(codigo, nombre, descripcion, estado, imagen, fechaCompra, Antiguedad, codigo1, categoria_codigo));
    }

    // TODO TERMINAR
    public static void listaritem() {

        String[] data = gestorI.listarItem();
        for (String s : data) {
            out.println(s);
        }
    }


    public static void insertarCategoria() throws Exception {
        String codigo;
        String nombre;
        out.println("Digite el codigo ");
        codigo = (in.readLine());
        out.println("Digite el nombre ");
        nombre = in.readLine();
       ControllerCategoria gestor = new ControllerCategoria();
        out.println(gestor.registrarCategoria(codigo, nombre));
    }

    public static void listarcategoria() throws Exception {

        ControllerCategoria gestor = new ControllerCategoria();
        String[] data = gestor.listarCategoria();
        for (String s : data) {
            out.println(s);
        }
    }


    /*  public static void insertarr() throws IOException {
        String codigo;
        LocalDate fechaCreacion, fechaInicio, FechaVencimiento;
        String estadoSubasta;
        double precioMinimoInicial;
        int horaInicio, horaVencimiento, dia,mes,year,dINi,mINi,yINi;
        out.println("Digite el codigo ");
        codigo = in.readLine();
        out.println("Digite la hora de inicio ");
        horaInicio = Integer.parseInt(in.readLine());
        out.println("Digite la hora de Vencimiento ");
        horaVencimiento = Integer.parseInt(in.readLine());
        out.println("Digite el dia de la fecha de creacion");
        dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes de la fecha de creacion");
        mes = Integer.parseInt(in.readLine());
        out.println("Digite el año de la fecha de creacion");
        year = Integer.parseInt(in.readLine());
        fechaCreacion= LocalDate.of(year,mes,dia);
        out.println("Digite el dia de la fecha de inicio");
        dINi = Integer.parseInt(in.readLine());
        out.println("Digite el mes de la fecha de inicio");
        mINi = Integer.parseInt(in.readLine());
        out.println("Digite el año de la fecha de inicio");
        yINi = Integer.parseInt(in.readLine());
        fechaInicio= LocalDate.of(yINi,mINi,dINi);
        out.println("Digite la fecha de inicio");
        FechaVencimiento = LocalDate.now();//parse(in.readLine());
        out.println("Digite el estado de la subasta");
        estadoSubasta = in.readLine();
        out.println("Digite el precio base ");
        precioMinimoInicial = Double.parseDouble(in.readLine());

        ControllerSubasta gestor = new ControllerSubasta();
        out.println(gestor.registrarSubasta(codigo,horaInicio,horaVencimiento,fechaCreacion,fechaInicio,FechaVencimiento,estadoSubasta,precioMinimoInicial));


    }
    public static void listar() {

        ControllerSubasta gestor = new ControllerSubasta();
        String[] data = gestor.listarSubasta();
        for (String s : data) {
            out.println(s);
        }
    }*/
}
